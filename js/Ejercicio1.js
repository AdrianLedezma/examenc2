const numBoleto = document.getElementById('txtnumBoleto');
    const numCliente = document.getElementById('txtnumCliente');
    const destino = document.getElementById('txtDestino');
    const tipoViaje = document.getElementById('cmbTipo');
    const precio = document.getElementById('txtPrecio');
    const subtotal = document.getElementById('txtSubtotal');
    const impuesto = document.getElementById('txtImpuesto');
    const totalPagar = document.getElementById('txtTotalPagar');

    const calcularButton = document.getElementById('Calcular');
    calcularButton.addEventListener('click', calcularTotal);

    function calcularTotal() {
        const numBoletoVal = parseFloat(numBoleto.value);
        const precioVal = parseFloat(precio.value);
        const tipoViajeVal = parseFloat(tipoViaje.value);

        const importe = numBoletoVal * precioVal;

        let subtotalVal = importe;
        if (tipoViajeVal === 2) {
            subtotalVal = importe * 1.8;
        }

        const impuestoVal = subtotalVal * 0.16;

        const totalPagarVal = subtotalVal + impuestoVal;

        subtotal.value = subtotalVal.toFixed(2);
        impuesto.value = impuestoVal.toFixed(2);
        totalPagar.value = totalPagarVal.toFixed(2);
    }

    const limpiarButton = document.getElementById('Limpiar');
    limpiarButton.addEventListener('click', limpiarCampos);

    function limpiarCampos() {
        numBoleto.value = '';
        numCliente.value = '';
        destino.value = '';
        tipoViaje.value = '1';
        precio.value = '';
        subtotal.value = '';
        impuesto.value = '';
        totalPagar.value = '';
    }